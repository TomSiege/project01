class Login {
  constructor(){
  }
   
  login(){
    
    let xhr = new XMLHttpRequest();
    xhr.open('POST','/login',true);
    xhr.setRequestHeader('Content-type','application/json');
    xhr.body
    xhr.onload = (response) => {
      let token = JSON.parse(response.target.response).token;
      console.log(token);
      localStorage.setItem('token',token)
    }
    xhr.send(JSON.stringify({
      id: 'tom',
      pw: 'feet'
    }));
  }

  getData(){
    let xhr = new XMLHttpRequest();
    xhr.open('GET','/data',true);
    xhr.setRequestHeader('Content-type','application/json');
    xhr.setRequestHeader('Authorization','Bearer '+localStorage.getItem('token'));
    xhr.body
    xhr.send({token: localStorage.getItem('token')});
  }
}
let login = new Login();
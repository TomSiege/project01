const express = require('express');
const app = express();
const assert = require('assert');
const MongoClient = require('mongodb').MongoClient;
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const url = 'mongodb://localhost:27017';
const dbName = 'users';
const port = 3000;

app.use(express.static('public'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/', (req, res, next) => {
  res.sendFile('./index.html', {root: __dirname});
})

app.get('/data', verifyToken, (req, res) => {
  const getData  = () => { 
    MongoClient.connect(url, function(err, client){
      assert.strictEqual(null, err);
      console.log('connected successfully to db')
      const db = client.db(dbName);
    
      const collection = db.collection('users');

      collection.find().toArray().then(docs => {
        console.log(docs);
        res.send(docs);
      });
    })
  }
  jwt.verify(req.token, 'blueball',(err, data)=> {
    if(err){
      res.sendStatus(403);
    } else {
      getData();
    }
  });

})

app.listen(port, ()=> {
  console.log('Listening to port 3000');
});


// LOGIN CODE

app.post('/login', checkUser, (req, res) =>{
  if(res.isValid){
    res.json({'token': res.token});
  } else {
    res.end('no');
  }
});

function checkUser(req, res, next){
  try{
    MongoClient.connect(url, function(err, client){
      assert.strictEqual(null, err);
       
      const db = client.db(dbName);
      const collection = db.collection('users');
      console.log(`looking up ${req.body.id} ${req.body.pw}`)
      collection.find({name: req.body.id, pw: req.body.pw}).toArray().then(docs => {
        if(docs.length === 1){
          let token = jwt.sign({name: req.body.id},'blueball',{expiresIn: '5s'});
            res.token = token;
            res.isValid = true;
            next();
            return;
        } else {
          res.isValid = false;
          next();
          return;
        };
      });
  
    })
  } catch( err ) {
  } finally {
 
  }
}

function verifyToken(req,res,next) {
  const bearerHeader = req.headers['authorization'];
  console.log(`bearerHeader ${bearerHeader}`);

  if (typeof bearerHeader !== undefined){
    const bearer = bearerHeader.split(' ');
    //get token from array
    const bearerToken = bearer[1];
    console.log(`bearerToken ${bearerToken}`);

    req.token = bearerToken;
    next();
  } else {
    res.sendStaus(403);
  }
}